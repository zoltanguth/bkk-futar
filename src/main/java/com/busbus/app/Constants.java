package com.busbus.app;

public interface Constants {
    
    String BASE_PACKAGE = "com.busbus.app.servlet.";
    String PUBSUB_TOPIC_NAME = "topic";
    
    String ROUTE_URL = "http://futar.bkk.hu/bkk-utvonaltervezo-api/ws/otp/api/where/vehicles-for-route.json?key=bkk-web&version=3&appVersion=2.3.3-20170810153906&routeId=BKK_0085&related=false";
    
    String GET_TRIP_STATS_URL_FIRST_PART = "http://futar.bkk.hu/bkk-utvonaltervezo-api/ws/otp/api/where/trip-details.json?tripId=";
    String GET_TRIP_STATS_URL_SECOND_PART = "&date=";
    String GET_TRIP_STATS_URL_THIRD_PART = "&key=bkk-web&version=3&appVersion=2.3.3-20170810153906";
    
    String DATASET_NAME = "Tripstats";
    String TABLE_NAME = "Trips";
    
    String BKK_TOPIC_NAME = "bkk-futar";
    
    String SUBSCRIPTION_NAME = "SaveTripStatsServlet";
    String SUBSCRIPTION_PATH = "/_ah/push-handlers/save_tripstats";
}
