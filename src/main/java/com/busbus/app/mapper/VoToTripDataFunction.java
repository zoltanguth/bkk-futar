package com.busbus.app.mapper;

import java.util.function.Function;

import com.busbus.app.model.TripData;
import com.busbus.app.vo.TripDataVo;

public enum VoToTripDataFunction implements Function<TripDataVo, TripData> {
    INSTANCE;
    
    @Override
    public TripData apply(TripDataVo tripDataVo) {
        return TripData.builder()
                       .tripEntry(VoToTripEntryFunction.INSTANCE.apply(tripDataVo.getEntry()))
                       .build();
    }
}
