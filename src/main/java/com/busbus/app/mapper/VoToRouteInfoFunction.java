package com.busbus.app.mapper;

import java.time.Instant;
import java.time.ZoneId;
import java.util.UUID;
import java.util.function.Function;

import com.busbus.app.model.RouteInfo;
import com.busbus.app.vo.RouteInfoVo;

public enum VoToRouteInfoFunction implements Function<RouteInfoVo, RouteInfo> {
    INSTANCE;
    
    
    
    @Override
    public RouteInfo apply(RouteInfoVo routeInfoVo) {
        
        Instant instant = Instant.ofEpochMilli(routeInfoVo.getCurrentTime());
        
        return RouteInfo.builder()
                        .id(UUID.randomUUID().toString())
                        .status(routeInfoVo.getStatus())
                        .version(routeInfoVo.getVersion())
                        .currentTime(instant.atZone(ZoneId.systemDefault()).toLocalDateTime())
                        .data(VoToRouteDataFunction.INSTANCE.apply(routeInfoVo.getData()))
                        .build();
    }
}
