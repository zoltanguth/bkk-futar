package com.busbus.app.mapper;

import java.util.function.Function;

import com.busbus.app.model.Trip;
import com.busbus.app.model.TripMessage;

public enum TripToTripMessageFunction implements Function<Trip, TripMessage> {
    INSTANCE;
    
    @Override
    public TripMessage apply(Trip trip) {
        return TripMessage.builder()
                          .tripId(trip.getTripId())
                          .serviceDate(trip.getServiceDate())
                          .build();
    }
}
