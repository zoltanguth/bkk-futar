package com.busbus.app.mapper;

import java.util.function.Function;

import com.busbus.app.model.TripStopTime;
import com.busbus.app.vo.TripStopTimeVo;

public enum VoToTripStopTimeFunction implements Function<TripStopTimeVo, TripStopTime> {
    INSTANCE;
    
    @Override
    public TripStopTime apply(TripStopTimeVo tripStopTimeVo) {
        return TripStopTime.builder()
                           .stopId(tripStopTimeVo.getStopId())
                           .arrivalTime(tripStopTimeVo.getArrivalTime())
                           .departureTime(tripStopTimeVo.getDepartureTime())
                           .predictedArrivalTime(tripStopTimeVo.getPredictedArrivalTime())
                           .predictedDepartureTime(tripStopTimeVo.getPredictedDepartureTime())
                           .stopSequence(tripStopTimeVo.getStopSequence())
                           .build();
    }
}
