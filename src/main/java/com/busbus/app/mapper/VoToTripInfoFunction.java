package com.busbus.app.mapper;

import java.util.function.Function;

import com.busbus.app.model.TripInfo;
import com.busbus.app.vo.TripInfoVo;

public enum VoToTripInfoFunction implements Function<TripInfoVo, TripInfo> {
    INSTANCE;
    
    @Override
    public TripInfo apply(TripInfoVo tripInfoVo) {
        return TripInfo.builder()
                       .status(tripInfoVo.getStatus())
                       .version(tripInfoVo.getVersion())
                       .currentTime(tripInfoVo.getCurrentTime())
                       .data(VoToTripDataFunction.INSTANCE.apply(tripInfoVo.getData()))
                       .build();
    }
}
