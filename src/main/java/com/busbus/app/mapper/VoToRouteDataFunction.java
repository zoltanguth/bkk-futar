package com.busbus.app.mapper;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.busbus.app.model.RouteData;
import com.busbus.app.vo.RouteDataVo;

public enum VoToRouteDataFunction implements Function<RouteDataVo, RouteData> {
    INSTANCE;
    
    @Override
    public RouteData apply(RouteDataVo routeDataVo) {
        return RouteData.builder()
                        .list(routeDataVo.getList().stream().map(tripVo -> VoToTripFunction.INSTANCE.apply(tripVo)).collect(Collectors.toList()))
                        .build();
    }
}
