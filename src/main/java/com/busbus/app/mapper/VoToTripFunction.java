package com.busbus.app.mapper;

import java.util.UUID;
import java.util.function.Function;

import com.busbus.app.model.TripState;
import com.busbus.app.model.Trip;
import com.busbus.app.vo.TripVo;

public enum VoToTripFunction implements Function<TripVo, Trip> {
    INSTANCE;
    
    @Override
    public Trip apply(TripVo tripVo) {
        return Trip.builder()
                   .routeId(tripVo.getRouteId())
                   .stopDistancePercent(tripVo.getStopDistancePercent())
                   .stopId(tripVo.getStopId())
                   .stopSequence(tripVo.getStopSequence())
                   .tripId(tripVo.getTripId())
                   .vehicleId(tripVo.getVehicleId())
                   .state(TripState.EN_ROUTE)
                   .serviceDate(tripVo.getServiceDate())
                   .build();
    }
}
