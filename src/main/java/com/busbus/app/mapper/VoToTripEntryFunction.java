package com.busbus.app.mapper;

import java.util.function.Function;
import java.util.stream.Collectors;

import com.busbus.app.model.TripEntry;
import com.busbus.app.vo.TripEntryVo;

public enum VoToTripEntryFunction implements Function<TripEntryVo, TripEntry> {
    INSTANCE;
    
    @Override
    public TripEntry apply(TripEntryVo entryVo) {
        return TripEntry.builder()
                        .tripId(entryVo.getTripId())
                        .serviceDate(entryVo.getServiceDate())
                        .stopTimes(entryVo.getStopTimes().stream().map(trip -> VoToTripStopTimeFunction.INSTANCE.apply(trip)).collect(Collectors.toList()))
                        .build();
    }
}
