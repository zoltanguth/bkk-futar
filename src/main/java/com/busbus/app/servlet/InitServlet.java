package com.busbus.app.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.busbus.app.Constants;
import com.busbus.app.service.BigQueryService;
import com.busbus.app.service.BigQueryServiceImpl;
import com.busbus.app.service.PubSubService;
import com.busbus.app.service.PubSubServiceImpl;

@WebServlet(name = "InitServlet", value = "/init")
public class InitServlet extends HttpServlet {
    
    PubSubService pubSubService = new PubSubServiceImpl();
    BigQueryService bigQueryService = new BigQueryServiceImpl();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        pubSubService.createTopic();
        pubSubService.createSubscription(Constants.SUBSCRIPTION_NAME, Constants.SUBSCRIPTION_PATH);
        
        bigQueryService.createTripDataset();
        bigQueryService.createTripTable();
        
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.getWriter().close();
    }
}
