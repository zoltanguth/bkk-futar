package com.busbus.app.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.busbus.app.mapper.VoToRouteInfoFunction;
import com.busbus.app.model.RouteInfo;
import com.busbus.app.model.Trip;
import com.busbus.app.service.DatastoreService;
import com.busbus.app.service.DatastoreServiceImpl;
import com.busbus.app.service.PubSubService;
import com.busbus.app.service.PubSubServiceImpl;
import com.busbus.app.service.UrlService;
import com.busbus.app.service.UrlServiceImpl;
import com.google.common.base.Strings;

@WebServlet(name = "GetRouteInfoServlet", value = "/getRouteInfo")
public class GetRouteInfoServlet extends HttpServlet {
    
    private DatastoreService datastoreService = new DatastoreServiceImpl();
    private PubSubService pubSubService = new PubSubServiceImpl();
    private UrlService urlService = new UrlServiceImpl();
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        RouteInfo routeInfo = VoToRouteInfoFunction.INSTANCE.apply(urlService.readInRouteInfoVo());
        
        datastoreService.saveRouteInfo(routeInfo);
        datastoreService.updateTrips(getLatestTripsMap(routeInfo));
        
        List<String> messageList = datastoreService.createMessageList();
        pubSubService.sendMessages(messageList);
        
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().close();
    }
    
    private Map<String, Trip> getLatestTripsMap(RouteInfo routeInfo) {
        return routeInfo.getData().getList().stream()
                        .filter(trip -> !Strings.isNullOrEmpty(trip.getTripId()))
                        .filter(trip -> !Strings.isNullOrEmpty(trip.getStopId()))
                        .collect(Collectors.toMap(Trip::getTripId, Function.identity()));
    }
}
