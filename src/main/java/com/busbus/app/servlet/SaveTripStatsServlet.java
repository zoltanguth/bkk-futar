package com.busbus.app.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.busbus.app.model.TripStopTime;
import com.busbus.app.service.BigQueryService;
import com.busbus.app.service.BigQueryServiceImpl;
import com.busbus.app.service.DatastoreService;
import com.busbus.app.service.DatastoreServiceImpl;
import com.busbus.app.service.UrlService;
import com.busbus.app.service.UrlServiceImpl;
import com.busbus.app.util.GsonParser;

public class SaveTripStatsServlet extends HttpServlet {
    
    private static final Logger log = Logger.getLogger(SaveTripStatsServlet.class.getName());
    
    private BigQueryService bigQueryService = new BigQueryServiceImpl();
    private DatastoreService datastoreService = new DatastoreServiceImpl();
    private UrlService urlService = new UrlServiceImpl();
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String message = GsonParser.parsePushMessage(req);
            List<TripStopTime> stopTimeList = urlService.getUpdatedStopTimesFromMessage(message);
            String tripId = message.split("\\.")[0];
                bigQueryService.insertTripList(stopTimeList);
                datastoreService.deleteTrip(tripId);
        } catch (Exception e) {
            log.warning("Error with SaveTripServlet"); //TODO check on other logger for error
            datastoreService.updateUploadFailedTripByTripId(req);
        }
        
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.getWriter().close();
    }
}
