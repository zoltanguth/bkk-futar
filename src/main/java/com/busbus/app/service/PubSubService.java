package com.busbus.app.service;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;

import com.google.api.services.pubsub.model.PubsubMessage;

public interface PubSubService {
    
    void publishMessage(PubsubMessage pubsubMessage) throws ServletException, IOException;
    
    void createTopic() throws IOException;
    
    void createSubscription(String subscriptionName, String subscriptionPath) throws IOException;
    
    void sendMessages(List<String> messages) throws ServletException, IOException;
    
}
