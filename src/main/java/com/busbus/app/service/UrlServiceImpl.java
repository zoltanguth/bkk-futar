package com.busbus.app.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.busbus.app.Constants;
import com.busbus.app.mapper.VoToTripInfoFunction;
import com.busbus.app.model.TripInfo;
import com.busbus.app.model.TripStopTime;
import com.busbus.app.util.GsonParser;
import com.busbus.app.vo.RouteInfoVo;
import com.busbus.app.vo.TripInfoVo;

public class UrlServiceImpl implements UrlService {
    
    @Override
    public TripInfoVo readInTripInfoVo(String tripId, String serviceDate) throws IOException {
        URL url = createUrlAddress(tripId, serviceDate);
        InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuffer json = new StringBuffer().append(reader.readLine());
        
        TripInfoVo tripInfoVo = GsonParser.parseBkkTripInfo(json);
        reader.close();
        
        return tripInfoVo;
    }
    
    @Override
    public RouteInfoVo readInRouteInfoVo() throws IOException {
        URL url = new URL(Constants.ROUTE_URL);
        InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuffer json = new StringBuffer().append(reader.readLine());
        
        RouteInfoVo routeInfoVo = GsonParser.parseBkkRouteInfo(json);
        reader.close();
        
        return routeInfoVo;
    }
    
    @Override
    public List<TripStopTime> getUpdatedStopTimesFromMessage(String message) throws IOException {
        String[] splitMessage = message.split("\\.");
        String tripId = splitMessage[0];
        String serviceDate = splitMessage[1];
        
        TripInfoVo infoVo = readInTripInfoVo(tripId, serviceDate);
        TripInfo tripInfo = VoToTripInfoFunction.INSTANCE.apply(infoVo);
        
        List<TripStopTime> stopTimes = tripInfo.getData().getTripEntry().getStopTimes();
        
        stopTimes.forEach(tripStopTime -> tripStopTime.setTripId(tripId));
        stopTimes.forEach(tripStopTime -> tripStopTime.setServiceDate(serviceDate));
        
        return stopTimes;
        
    }
    
    private URL createUrlAddress(String tripId, String serviceDate) throws MalformedURLException {
        String urlAddress = Constants.GET_TRIP_STATS_URL_FIRST_PART + tripId + Constants.GET_TRIP_STATS_URL_SECOND_PART + serviceDate + Constants.GET_TRIP_STATS_URL_THIRD_PART;
        return new URL(urlAddress);
    }
}
