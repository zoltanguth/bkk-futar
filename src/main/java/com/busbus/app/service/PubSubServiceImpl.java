package com.busbus.app.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.ServletException;

import com.busbus.app.Constants;
import com.busbus.app.util.PubsubUtil;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpStatusCodes;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.PushConfig;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

public class PubSubServiceImpl implements PubSubService {
    
    private Pubsub client;
    private DatastoreService datastoreService = new DatastoreServiceImpl();
    private static final Logger log = Logger.getLogger(PubSubService.class.getName());
    
    @Override
    public void createTopic() throws IOException {
        initPubsub();
        String name = getfullTopicName(Constants.BKK_TOPIC_NAME);
        try {
            Topic topic = client.projects().topics().get(name).execute();
            if (topic != null) {
                throw new RuntimeException("Topic already exists! Topic name: " + Constants.BKK_TOPIC_NAME);
            }
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
                client.projects().topics()
                      .create(name, new Topic())
                      .execute();
            } else {
                throw e;
            }
        }
    }
    
    @Override
    public void createSubscription(String subscriptionName, String subscriptionPath) throws IOException {
        initPubsub();
        String uniqueSubscriptionName = Constants.BKK_TOPIC_NAME + "-" + subscriptionName;
        String fullName = getFullSubscriptionName(uniqueSubscriptionName);
        try {
            Subscription subscription = client.projects().subscriptions().get(fullName).execute();
            if (subscription != null) {
                throw new RuntimeException("Subscription already exists! Subscription name: " + subscriptionName);
            }
        } catch (GoogleJsonResponseException e) {
            if (e.getStatusCode() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
                String fullTopicName = getfullTopicName(Constants.BKK_TOPIC_NAME);
                PushConfig pushConfig = new PushConfig()
                        .setPushEndpoint(getAppEndpointUrl(subscriptionPath, subscriptionName + ".UniqueToken"));
                Subscription subscription = new Subscription()
                        .setTopic(fullTopicName)
                        .setPushConfig(pushConfig);
                client.projects().subscriptions()
                      .create(fullName, subscription)
                      .execute();
            } else {
                throw e;
            }
        }
    }
    
    @Override
    public void publishMessage(PubsubMessage message) throws ServletException, IOException {
        initPubsub();
        String fullTopicName = getfullTopicName(Constants.BKK_TOPIC_NAME);
        
        Map<String, String> attributes = Maps.newHashMap();
        attributes.put(Constants.PUBSUB_TOPIC_NAME, fullTopicName);
        message.setAttributes(attributes);
        
        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setMessages(ImmutableList.of(message));
        
        client.projects().topics()
              .publish(fullTopicName, publishRequest)
              .execute();
    }
    
    @Override
    public void sendMessages(List<String> messages) throws ServletException, IOException {
        for (String message : messages) {
            PubsubMessage pubsubMessage = new PubsubMessage();
            pubsubMessage.encodeData(message.getBytes("UTF-8"));
            publishMessage(pubsubMessage);
            log.info("Trip " + message + " sent to Pubsub");
        }
    }
    
    private void initPubsub() {
        if (client == null) {
            try {
                client = PubsubUtil.getClient();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    private String getAppEndpointUrl(String path, String tokenName) {
        String token = System.getProperty(Constants.BASE_PACKAGE + tokenName);
        return "https://" + PubsubUtil.getProjectId()
                + ".appspot.com" + path + "?token=" + token;
    }
    
    private String getFullSubscriptionName(String subscriptionName) {
        return String.format("projects/%s/subscriptions/%s",
                             PubsubUtil.getProjectId(),
                             subscriptionName);
    }
    
    private String getfullTopicName(String topicName) {
        return String.format("projects/%s/topics/%s",
                             PubsubUtil.getProjectId(),
                             topicName);
    }
    
}
