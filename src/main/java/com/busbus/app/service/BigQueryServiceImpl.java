package com.busbus.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.busbus.app.Constants;
import com.busbus.app.model.TripStopTime;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.Dataset;
import com.google.cloud.bigquery.DatasetInfo;
import com.google.cloud.bigquery.Field;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.LegacySQLTypeName;
import com.google.cloud.bigquery.Schema;
import com.google.cloud.bigquery.StandardTableDefinition;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableInfo;

public class BigQueryServiceImpl implements BigQueryService {
    
    private static final Logger log = Logger.getLogger(BigQueryServiceImpl.class.getName());
    
    BigQuery bigQuery = BigQueryOptions.getDefaultInstance().getService();
    
    @Override
    public void insertTripList(List<TripStopTime> tripStopTime) {
        TableId tableId = TableId.of(Constants.DATASET_NAME, Constants.TABLE_NAME);
        InsertAllRequest.Builder allRequest = prepareRequestBuilder(tripStopTime, tableId);
        InsertAllResponse response = bigQuery.insertAll(allRequest.build());
        
        if (!response.hasErrors() && !tripStopTime.isEmpty()) {
            log.info("All stop times have been successfully uploaded to bigquery for Trip: " + tripStopTime.get(0).getTripId());
        } else if (!tripStopTime.isEmpty()) {
            log.info("Error occured while uploading stoptimes to bigquery for Trip: " + tripStopTime.get(0).getTripId());
        }
    }
    
    @Override
    public void createTripDataset() {
        Dataset dataset = bigQuery.getDataset(Constants.DATASET_NAME);
        if (dataset == null) {
            bigQuery.create(DatasetInfo.newBuilder(Constants.DATASET_NAME).build());
        }
        log.info("Bigquery dataset created successfully");
    }
    
    @Override
    public void createTripTable() {
        if (bigQuery.getDataset(Constants.DATASET_NAME).get(Constants.TABLE_NAME) == null) {
            
            TableId tableId = TableId.of(Constants.DATASET_NAME, Constants.TABLE_NAME);
            StandardTableDefinition tableDefinition = StandardTableDefinition.of(getSchema());
            bigQuery.create(TableInfo.of(tableId, tableDefinition));
            
            log.info("Bigquery table created successfully");
        }
    }
    
    private Schema getSchema() {
        Field tripId = Field.of("tripId", LegacySQLTypeName.STRING);
        Field serviceDate = Field.of("serviceDate", LegacySQLTypeName.STRING);
        Field stopId = Field.of("stopId", LegacySQLTypeName.STRING);
        Field arrivalTime = Field.of("arrivalTime", LegacySQLTypeName.INTEGER);
        Field departureTime = Field.of("departureTime", LegacySQLTypeName.INTEGER);
        Field predictedArrivalTime = Field.of("predictedArrivalTime", LegacySQLTypeName.INTEGER);
        Field predictedDepartureTime = Field.of("predictedDepartureTime", LegacySQLTypeName.INTEGER);
        Field stopSequence = Field.of("stopSequence", LegacySQLTypeName.INTEGER);
        
        return Schema.of(tripId, serviceDate, stopId, arrivalTime, departureTime, predictedArrivalTime, predictedDepartureTime, stopSequence);
    }
    
    private InsertAllRequest.Builder prepareRequestBuilder(List<TripStopTime> tripStopTime, TableId tableId) {
        InsertAllRequest.Builder allRequest = InsertAllRequest.newBuilder(tableId);
        for (TripStopTime stopTime : tripStopTime) {
            Map<String, Object> rowContent = fillRow(stopTime);
            allRequest.addRow(rowContent);
            log.info("Trip " + stopTime.getTripId() + stopTime.getStopSequence() + " is loaded to bigquery");
        }
        return allRequest;
    }
    
    private Map<String, Object> fillRow(TripStopTime tripStopTime) {
        Map<String, Object> rowContent = new HashMap<>();
        rowContent.put("tripId", tripStopTime.getTripId());
        rowContent.put("serviceDate", tripStopTime.getServiceDate());
        rowContent.put("stopId", tripStopTime.getStopId());
        
        if (tripStopTime.getArrivalTime() == null) {
            rowContent.put("arrivalTime", 0);
            rowContent.put("predictedArrivalTime", 0);
        } else {
            rowContent.put("arrivalTime", tripStopTime.getArrivalTime());
            rowContent.put("predictedArrivalTime", tripStopTime.getPredictedArrivalTime());
        }
        
        if (tripStopTime.getDepartureTime() == null) {
            rowContent.put("departureTime", 0);
            rowContent.put("predictedDepartureTime", 0);
        } else {
            rowContent.put("departureTime", tripStopTime.getDepartureTime());
            rowContent.put("predictedDepartureTime", tripStopTime.getPredictedDepartureTime());
        }
        rowContent.put("stopSequence", tripStopTime.getStopSequence());
        
        return rowContent;
    }
}
