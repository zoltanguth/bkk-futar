package com.busbus.app.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.busbus.app.model.TripStopTime;
import com.busbus.app.vo.RouteInfoVo;
import com.busbus.app.vo.TripInfoVo;

public interface UrlService {
    
    TripInfoVo readInTripInfoVo(String tripId, String serviceDate) throws IOException;
    
    RouteInfoVo readInRouteInfoVo() throws IOException;
    
    List<TripStopTime> getUpdatedStopTimesFromMessage(String message) throws IOException;
    
}
