package com.busbus.app.service;

import java.util.List;

import com.busbus.app.model.TripStopTime;

public interface BigQueryService {
    
    void insertTripList(List<TripStopTime> tripStopTime);
    
    void createTripDataset();
    
    void createTripTable();
}
