package com.busbus.app.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.busbus.app.model.RouteInfo;
import com.busbus.app.model.Trip;
import com.busbus.app.model.TripState;
import com.googlecode.objectify.cmd.Query;

public interface DatastoreService {
    
    void saveRouteInfo(RouteInfo routeInfo);
    
    void saveTrips(List<Trip> trips);
    
    void saveTrip(Trip trip);
    
    void deleteTrip(String tripId);
    
    Query<Trip> getTripsByTripId(String tripId);
    
    List<Trip> getTripsWithStateOf(TripState state);
    
    void updateTrips(Map<String, Trip> tripMap);
    
    List<String> createMessageList();
    
    void updateTripSate(TripState state, Trip trip);
    
    void updateUploadFailedTripByTripId(HttpServletRequest request) throws IOException;
}
