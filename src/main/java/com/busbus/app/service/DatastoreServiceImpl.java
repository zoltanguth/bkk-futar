package com.busbus.app.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.busbus.app.model.RouteInfo;
import com.busbus.app.model.Trip;
import com.busbus.app.model.TripState;
import com.busbus.app.util.GsonParser;
import com.google.common.collect.Lists;
import com.googlecode.objectify.cmd.Query;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class DatastoreServiceImpl implements DatastoreService {
    
    private static final Logger log = Logger.getLogger(DatastoreService.class.getName());
    
    @Override
    public void saveRouteInfo(RouteInfo routeInfo) {
        ofy().save().entity(routeInfo).now();
    }
    
    @Override
    public void saveTrips(List<Trip> trips) {
        trips.stream().forEach(this::saveTrip);
    }
    
    @Override
    public void saveTrip(Trip trip) {
        if (trip.getId() == null) {
            trip.setId(UUID.randomUUID().toString());
        }
        ofy().save().entity(trip).now();
        log.info("Trip " + trip.getId() + " saved");
    }
    
    @Override
    public List<String> createMessageList() {
        return getTripsWithStateOf(TripState.ARRIVED_UPDATING).stream()
                                                              .map(trip -> String.format("%s.%s", trip.getTripId(), trip.getServiceDate()))
                                                              .collect(Collectors.toList());
    }
    
    @Override
    public List<Trip> getTripsWithStateOf(TripState state) {
        Query<Trip> savedTrips = ofy().load().type(Trip.class);
        return savedTrips.list().stream()
                         .filter(trip -> trip.getState().equals(state))
                         .collect(Collectors.toList());
    }
    
    @Override
    public void updateTrips(Map<String, Trip> freshTripMap) {
        List<Trip> savedTrips = getTripsWithStateOf(TripState.EN_ROUTE);
        if (savedTrips.isEmpty()) {
            freshTripMap.values().stream().forEach(this::saveTrip);
        } else {
            Map<String, String> savedTripIdsMap = savedTrips.stream().collect(Collectors.toMap(Trip::getTripId, Trip::getId));
            
            saveNewTrips(freshTripMap, savedTripIdsMap);
            updateTripsStateToArrived(freshTripMap, savedTrips);
            updateOngoingTripDetailsAndSave(freshTripMap, savedTripIdsMap);
        }
    }
    
    @Override
    public void updateTripSate(TripState state, Trip trip) {
        trip.setState(state);
        saveTrip(trip);
        log.info("Trip" + trip.getTripId() + " state is updated to " + state.name());
    }
    
    @Override
    public void deleteTrip(String tripId) {
        getTripsByTripId(tripId).forEach(trip -> ofy().delete().entity(trip));
        log.info("Trip deleted : " + tripId);
    }
    
    @Override
    public Query<Trip> getTripsByTripId(String tripId) {
        return ofy().load().type(Trip.class).filter("tripId =", tripId);
    }
    
    @Override
    public void updateUploadFailedTripByTripId(HttpServletRequest req) {
        try {
            String message = GsonParser.parsePushMessage(req);
            String tripId = message.split("\\.")[0];
            getTripsByTripId(tripId).forEach(trip -> updateTripSate(TripState.ERROR_WHILE_UPDATING, trip));
        } catch (Exception e) {
            log.info("Error whit status update to ERROR_WHILE_UPLOADING");
        }
    }
    
    private void updateTripsStateToArrived(Map<String, Trip> freshTripMap, List<Trip> savedTrips) {
        savedTrips.stream().filter(trip -> !freshTripMap.keySet().contains(trip.getTripId())).forEach(trip -> updateTripSate(TripState.ARRIVED_UPDATING, trip));
    }
    
    private void updateOngoingTripDetailsAndSave(Map<String, Trip> freshTripMap, Map<String, String> savedTripIdsMap) {
        freshTripMap.values().stream()
                    .filter(trip -> savedTripIdsMap.keySet().contains(trip.getTripId()))
                    .forEach(trip -> trip.setId(savedTripIdsMap.get(trip.getTripId())));
        
        freshTripMap.values().stream().forEach(this::saveTrip);
        
        saveTrips(Lists.newArrayList(freshTripMap.values()));
    }
    
    private void saveNewTrips(Map<String, Trip> freshTripMap, Map<String, String> savedTripIdsMap) {
        freshTripMap.keySet().stream()
                    .filter(id -> !savedTripIdsMap.keySet().contains(id))
                    .forEach(id -> saveTrip(freshTripMap.get(id)));
    }
    
}
