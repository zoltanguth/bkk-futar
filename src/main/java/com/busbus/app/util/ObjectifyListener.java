package com.busbus.app.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.busbus.app.model.RouteInfo;
import com.busbus.app.model.Trip;
import com.googlecode.objectify.ObjectifyService;

public class ObjectifyListener implements ServletContextListener {
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ObjectifyService.register(RouteInfo.class);
        ObjectifyService.register(Trip.class);
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    
    }
}
