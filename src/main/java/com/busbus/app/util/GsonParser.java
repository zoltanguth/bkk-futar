package com.busbus.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import com.busbus.app.Constants;
import com.busbus.app.model.TripMessage;
import com.busbus.app.vo.RouteInfoVo;
import com.busbus.app.vo.TripInfoVo;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.apache.commons.io.IOUtils;

public class GsonParser {
    
    public static final GsonBuilder GSON_BUILDER = new GsonBuilder();
    
    private static final Gson GSON = gsonWithNamingStrategy(FieldNamingPolicy.IDENTITY);
    
    public static RouteInfoVo parseBkkRouteInfo(StringBuffer json) {
       return GSON_BUILDER.create().fromJson(json.toString(), RouteInfoVo.class);
    }
    
    public static TripInfoVo parseBkkTripInfo(StringBuffer json) {
        return GSON_BUILDER.create().fromJson(json.toString(), TripInfoVo.class);
    }
    
    public static PubsubMessage parsePubsubMessage(HttpServletRequest req) throws IOException {
        String messageString = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
        JsonObject jsonObject = GSON.fromJson(messageString, JsonObject.class);
        return GSON.fromJson(jsonObject.get("message"), PubsubMessage.class);
    }
    
    public static String parsePushMessage(HttpServletRequest req) throws IOException {
        PubsubMessage pubsubMessage = parsePubsubMessage(req);
        return GSON.fromJson(new String(pubsubMessage.decodeData(), "UTF-8"), String.class);
    }
    
    private static Gson gsonWithNamingStrategy(FieldNamingStrategy strategy) {
        return GSON_BUILDER.setFieldNamingStrategy(strategy).create();
    }
}
