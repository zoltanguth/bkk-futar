package com.busbus.app.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonProperty;


@Getter
@Setter
@ToString
@Builder
public class RouteInfoVo {
    
    @JsonProperty(value = "version")
    private Integer version;
    
    @JsonProperty(value = "status")
    private String status;
    
    @JsonProperty(value = "currentTime")
    private Long currentTime;

    @JsonProperty("data")
    private RouteDataVo data;
}

