package com.busbus.app.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@Builder
@ToString
public class TripDataVo {
    
    @JsonProperty(value = "entry")
    private TripEntryVo entry;
    
}
