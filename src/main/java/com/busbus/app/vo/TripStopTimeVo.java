package com.busbus.app.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@ToString
@Builder
public class TripStopTimeVo {
    
    @JsonProperty(value = "stopId")
    private String stopId;
    
    @JsonProperty(value = "arrivalTime")
    private Integer arrivalTime;
    
    @JsonProperty(value = "departureTime")
    private Integer departureTime;
    
    @JsonProperty(value = "predictedArrivalTime")
    private Integer predictedArrivalTime;
    
    @JsonProperty(value = "predictedDepartureTime")
    private Integer predictedDepartureTime;
    
    @JsonProperty(value = "stopSequence")
    private Integer stopSequence;
}
