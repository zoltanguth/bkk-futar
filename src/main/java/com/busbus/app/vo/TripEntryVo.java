package com.busbus.app.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@Builder
@ToString
public class TripEntryVo {
    
    @JsonProperty(value = "tripId")
    private String tripId;
    
    @JsonProperty(value = "serviceDate")
    private String serviceDate;
    
    @JsonProperty(value = "stopTimes")
    private List<TripStopTimeVo> stopTimes;
}
