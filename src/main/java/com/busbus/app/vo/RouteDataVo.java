package com.busbus.app.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@Builder
@ToString
public class RouteDataVo {
    
    @JsonProperty("list")
    private List<TripVo> list;
}
