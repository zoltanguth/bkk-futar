package com.busbus.app.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@Builder
@ToString
public class TripInfoVo {
    
    @JsonProperty(value = "version")
    private Integer version;
    
    @JsonProperty(value = "status")
    private String status;
    
    @JsonProperty(value = "currentTime")
    private Long currentTime;
    
    @JsonProperty(value = "data")
    private TripDataVo data;
    
}
