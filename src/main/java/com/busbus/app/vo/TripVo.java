package com.busbus.app.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
@ToString
@Builder
public class TripVo {
    
    @JsonProperty(value = "vehicleId")
    private String vehicleId;
    
    @JsonProperty(value = "stopId")
    private String stopId;
    
    @JsonProperty(value = "stopSequence")
    private Integer stopSequence;
    
    @JsonProperty(value = "routeId")
    private String routeId;
    
    @JsonProperty(value = "stopDistancePercent")
    private Integer stopDistancePercent;
    
    @JsonProperty(value = "tripId")
    private String tripId;
    
    @JsonProperty(value = "serviceDate")
    private String serviceDate;
}
