package com.busbus.app.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class TripStopTime {
    
    private String tripId;
    
    private String serviceDate;
    
    private String stopId;
    
    private Integer arrivalTime;
    
    private Integer departureTime;
    
    private Integer predictedArrivalTime;
    
    private Integer predictedDepartureTime;
    
    private Integer stopSequence;
}
