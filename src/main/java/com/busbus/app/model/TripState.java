package com.busbus.app.model;

public enum TripState {
    
    EN_ROUTE,
    ARRIVED_UPDATING,
    ERROR_WHILE_UPDATING;
}
