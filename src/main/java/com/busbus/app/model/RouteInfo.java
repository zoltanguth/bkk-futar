package com.busbus.app.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
@Getter
@Setter
@ToString
@Builder
public class RouteInfo {
    
    @Id
    private String id;
    private Integer version;
    private String status;
    private LocalDateTime currentTime;
    private RouteData data;
}
