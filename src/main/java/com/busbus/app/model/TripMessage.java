package com.busbus.app.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class TripMessage {
    
    private String tripId;
    private String serviceDate;
    
}
