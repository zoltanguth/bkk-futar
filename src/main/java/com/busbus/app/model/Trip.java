package com.busbus.app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Nullable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trip {
    
    @Id
    private String id;
    private String routeId;
    @Index
    private String tripId;
    private String vehicleId;
    private String stopId;
    private Integer stopSequence;
    private Integer stopDistancePercent;
    @Index
    private TripState state;
    private String serviceDate;
}
