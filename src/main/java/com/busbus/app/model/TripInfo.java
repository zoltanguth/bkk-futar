package com.busbus.app.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class TripInfo {
    
    private Integer version;
    
    private String status;
    
    private Long currentTime;
    
    private TripData data;
}
